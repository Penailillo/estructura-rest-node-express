const {Schema, model} = require('mongoose');

const UsuarioSchema = Schema({
    nombre: {
        type: String, 
        required: [true, 'SCHEMA -El nombre es obligatorio']
    },
    correo: {
        type: String, 
        required: [true, 'El correo es obligatorio'],
        unique: true
    },
    password: {
        type: String, 
        required: [true, 'La contraseña es obligatoria']
    },
    imagen: {
        type: String,
    },
    role: {
        type: String, 
        required: true,
        default: 'USER_ROLE',
        enum: ['ADMIN_ROLE', 'USER_ROLE']
    },
    estado: {
        type: Boolean, 
        default: true,
    },
    google: {
        type: Boolean, 
        default: false,
    },
});

// Siempre usar funcion antigua por el uso del this, esto es para quitar los elementos que no quiero mostrar cuando haga un toJSON
UsuarioSchema.methods.toJSON = function() {
    const {__v, password, _id, ...user} = this.toObject();
    user.uid = _id;
    return user;
}

module.exports = model('Usuario', UsuarioSchema);