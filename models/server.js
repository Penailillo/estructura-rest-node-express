const express = require('express')
const cors = require('cors');
const {dbConnection} = require('../db/config')

class Server {

    constructor() {
        this.app = express();
        this.port = process.env.PORT || 8180;

        this.usuariosRoutePath = '/api/usuarios'; // ruta base
        this.authLoginRoutePath = '/api/auth';

        // Conectar a base de datos:
        this.conectarDB();

        //middlewares:
        this.middlewares();

        // rutas aplicacion
        this.routes();
    }

    async conectarDB(){
        // dejar aca las conexiones a produccion, desarrollo, qa, etc
        await dbConnection();
    }

    middlewares() {
        // use => Palabra clave para saber que es un middleware
        // Configuracion de CORS
        this.app.use(cors());

        // Parseo y lectura del body en request
        // Cualquier informacion que venga la va a intentar serializar a JSON
        this.app.use(express.json())

        // directorio publico
        // cuando se hace esto, toma prestado el '/' del get
        this.app.use(express.static('public'))
    }

    routes() {
        // en el middleware se define el nuevo path a utilizar
        this.app.use(this.usuariosRoutePath, require('../routes/usuarios'))
        this.app.use(this.authLoginRoutePath, require('../routes/auth'))
    }

    listen() {
        this.app.listen(this.port, () => console.log(`Example app listening on ${this.port} port!`))
    }
}

module.exports = Server;