const mongoose = require('mongoose');

const dbConnection = async () => {
    try {
        // await porque el connect me devuelve una promesa. Asi espero que se conecte la db
        await mongoose.connect(process.env.MONGODB_ATLAS, {
            useNewUrlParser: true, 
            useUnifiedTopology: true,
            useCreateIndex: true,
            useFindAndModify: false
        });

        console.log('Base de datos ONLINE')
    } catch (error) {
        console.log(error);
        throw new Error('Error al iniciar la base de datos');
    }
}


module.exports = {
    dbConnection
}