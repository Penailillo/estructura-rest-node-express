const { response, request } = require('express');
const jwt = require('jsonwebtoken');
const { usuariosDelete } = require('../controllers/usuarios');
const Usuario = require('../models/usuario')


const validarJWT = async(req = request, res=response, next) => {

    // Obtengo el token (por nombre) que viene en la cabecera
    const token = req.header('x-token');

    if(!token) {
        return res.status(401).json({
            msg: 'El token esta vacio'
        })
    }

    try {
        
        const {uid} = jwt.verify(token, process.env.SECRETORPRIVATEKEY);
        const usuario = await Usuario.findById(uid);

        if(!usuario) {
            return res.status(401).json({
                msg: 'Error al verificar token - Usuario no existe en bd'
            })
        }

        // Verificar si el uid no esta eliminado (estado = true)
        if(!usuario.estado) {
            return res.status(401).json({
                msg: 'Token no valido - usuario con estado false'
            })
        }
        
        req.uid = uid;
        req.usuario = usuario;
        next();
    } catch (error) {
        console.log(error)
        res.status(401).json({
            msg: 'Error al verificar token'
        })
    }
    
    
}

module.exports = {
    validarJWT
}