const { request, response } = require("express")

const isAdminRole = (req = request, res = response, next) => {

    // no tiene try catch ya que no estoy ejecutando ninguna accion, solo validando si viene objetos y atributos
    if(!req.usuario) {
        return res.status(500).json({
            msg: 'Se quiere verificar el role sin validar el token primero'
        })
    }

    const {role, nombre} = req.usuario;

    if(role !== 'ADMIN_ROLE') {
        return res.status(401).json({
            msg: `${nombre} no tiene rol de administrador, no puede realizar esta accion`
        })
    }

    next();
}

const tieneRole = (...roles) => {

    // Aqui tampoco estoy ejecutando acciones, sino que estoy validando si vienen objetos o atributos

    // Retorno la funcion que se ejecutara en mis rutas
    return (req, res, next) => {
        if(!req.usuario) {
            return res.status(500).json({
                msg: 'Se quiere verificar el role sin validar el token primero'
            })
        }

        if(!roles.includes(req.usuario.role)) {
            return res.status(401).json({
                msg: `El servicio requiere uno de estos roles ${roles}`
            })
        }


        next();
    }
}

module.exports = {
    isAdminRole,
    tieneRole
}