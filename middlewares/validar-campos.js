const { validationResult } = require('express-validator');


const validarCampos = (req, res, next) => {
    const errors = validationResult(req);

    if(!errors.isEmpty()) {
        return res.status(400).json(errors);
    }

    // Si llega hasta aca, significa que debe pasar al siguiente middleware
    next();
}


module.exports = {
    validarCampos
}