const jwt = require('jsonwebtoken')

const generarJWT = (uid) => {
    
    return new Promise((resolve, reject) => {
        // no grabar informacion importante en un jwt

        const payload = {uid};

        // Se firma el jwt con el payload definido, se crea una llave en el archivo de env, se puede agregar opciones de expiracion y por ultimo, recibo un callback con el error y el token
        jwt.sign(payload, process.env.SECRETORPRIVATEKEY, {expiresIn: '4h'}, (err, token) => {
            if(err) {
                console.log('error al generar token', err)
                reject('No se pudo generar el token')
            } else {
                resolve(token)
            }
        })
    })

}

module.exports = {
    generarJWT
}