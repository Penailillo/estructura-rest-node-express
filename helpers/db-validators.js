const Role = require('../models/role');
const Usuario = require('../models/usuario');

const esRolValido = async (role = '') =>  {
    const existeRole = await Role.findOne({role});
    if(!existeRole) {
        throw new Error(`El rol : ${role} no esta registrado en la bd`);
    }
}


const emailExist = async (correo = '') => {
    const existeCorreo = await Usuario.findOne({ correo });
    if(existeCorreo) {
        throw new Error(`El correo : ${correo} ya esta registrado en la bd`);
    }
}

const existeUsuarioPorId = async(id) => {
    const existeId = await Usuario.findById(id);
    if(!existeId) {
        throw new Error(`El id : ${id} no existe`);
    }
}


module.exports = {
    esRolValido,
    emailExist,
    existeUsuarioPorId
}