const {response} = require('express');
const bcryptjs = require('bcryptjs');
const Usuario = require('../models/usuario');


const usuariosGet = async(req, res = response) => {
    
    // req.query = endpoint de forma: http://localhost:8081/api/usuarios?nombre=matias&apikey=122312
    // const query = req.query; traigo toa la wea
    // Es mejor desestructurar los query ya que si no viene uno de los parametros en la query, le podemos dejar el valor por defecto, revisar apikey
    const {limite = 5, desde = 0} = req.query;
    const query = {estado: true}

    // const usuarioList = await Usuario.find(query).skip(Number(desde)).limit(Number(limite));
    // const totalRegistros = await Usuario.countDocuments(query);

    // Se pone el await para esperar que todas terminen en su momento y no desfasado 
    // Con promise.all se llama en simultaneo las promesas, en cambio, si se llama por await como estaba arriba, una se demora mas y la otra menos y se desfasa
    const [total, usuarios] = await Promise.all([
        await Usuario.countDocuments(query),
        await Usuario.find(query).skip(Number(desde)).limit(Number(limite))
    ])


    res.json({
        total,
        usuarios,
    })
}

const usuariosPost = async(req, res=response) => {


    // Cuando se ocupe un post, se obtiene la data desde: req.body de ahi se puede desestructurar o sacar todo el contenido de una
    // En el caso de mongoose, puedo mandarle el req.body y el parsea y setea los valores que concuerdan con el schema definido (llena el objeto), dejando afuera los atributos que no estan

    //instancio mi Schema Usuario
    //extraer solo lo obligatorio, si quieres extraer el resto, usar ...nombre
    const {nombre, correo, password, role, ...resto} = req.body;
    //const {...test} = req.body;

    // resto, extrae todo lo faltante del objeto en caso que se hayan extraido mas propiedades
    //console.log('resto', resto)

    // test, abre y extrae completamente el objeto ya que no extrajo ninguna propiedad antes
    //console.log('test', test)

    //opcion 1
    //const usuario = new Usuario(req.body);

    //opcion2: 
    const usuario = new Usuario({nombre, correo, password, role});

    // Verificar Correo en bd
    

    //Encriptar DB
    const salt = bcryptjs.genSaltSync();
    usuario.password = bcryptjs.hashSync(password, salt);


    console.log('Objeto Usuario Mongoose', usuario);
    
    // save en db:
    // el save de mongoose devuelve una promesa, por lo que si quiero usarlo con async/await debo controlarlo con un try catch
    await usuario.save();


    res.json({
        msg: 'POST API - Controller',
        usuario
    })
}

const usuariosPut = async(req, res=response) => {
    // req.params = endpoint de forma: http://localhost:8081/api/usuarios/10
    //const id = req.params.id;
    // o
    // const { id } = req.params; se destructura segun la cantidad de parametros definidos en el endpoint (revisar usuarios route) hacer console.log a req.params
    const { id } = req.params; // el parametro
    const {_id, password, google, correo, ...resto} = req.body; // el objeto modificado que viene, extraigo los que no quiero actualizar y actualizo solo el "resto"
    
    // Validar contra base de datos
    if(password) {
        //Encriptar DB
        const salt = bcryptjs.genSaltSync();
        resto.password = bcryptjs.hashSync(password, salt);
    }

    const usuario = await Usuario.findByIdAndUpdate(id, resto);

    res.json(usuario)
}

const usuariosPatch = (req, res=response) => {
    res.json({
        msg: 'PATCH API - Controller'
    })
}

const usuariosDelete = async (req, res=response) => {
    const {id} = req.params;

    // Elimina registro de la bd y me devuelve el usuario eliminado
    // const usuario = await Usuario.findByIdAndDelete(id); 

    const usuario = await Usuario.findByIdAndUpdate(id, {estado: false}); 

    res.json(usuario);
}


module.exports = {
    usuariosGet,
    usuariosPost,
    usuariosPut,
    usuariosPatch,
    usuariosDelete,
}