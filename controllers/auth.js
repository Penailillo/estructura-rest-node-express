const {response} = require('express');
const Usuario = require('../models/usuario')
const bcryptjs = require('bcryptjs');
const { generarJWT } = require('../helpers/generar-jwt');
const { googleVerify } = require('../helpers/google-verify')

// Es un endpoint aparte
const login = async (req, res = response) => {
    const {correo, password} = req.body;

    try {

        // me traigo el usuario si esque el correo existe
        const usuario = await Usuario.findOne({correo});

        if(!usuario) {
            return res.status(400).json({
                msg: 'Usuario / Password incorrecto'
            })
        }   

        // Verificar si el usuario esta activo en bd

        if(usuario.estado === false) {
            return res.status(400).json({
                msg: 'Usuario / Password incorrecto'
            })
        }

        // verificar la contraseña, (verifico si la contraseña que me mandan es la misma que la que tengo en bd)
        const passwordValido = bcryptjs.compareSync(password, usuario.password)
        if(!passwordValido){
            return res.status(400).json({
                msg: 'Usuario / Password incorrecto (password)'
            })
        }

        // generar jwt
        const token = await generarJWT(usuario.id);
        
        res.json({
           usuario,
           token
        })
        
    } catch (error) {
        console.log(error)
        res.status(500).json({
            msg: 'Algo salió mal al intentar logearse'
        })
    }
    
}


const googleSignIn = async (req = request, res = response) => {

    const {id_token} = req.body;

    try{

        const {nombre, correo, img} = await googleVerify(id_token);

        // Verificar si el correo existe en la bd, de existir, me traigo el usuario
        let usuario = await Usuario.findOne({correo});

        if(!usuario) {
            //si no existe, lo creo
            const data = {
                nombre,
                correo,
                img,
                password: ':P',
                img,
                google: true
            };

            usuario = new Usuario(data)
            await usuario.save();
        }

        if(!usuario.estado) {
            // Si el usuario ingresa por google pero existe y mas encima esta con estado false, no se deberia dejar pasar
            return res.status(401).json({
                msg: 'Usuario bloqueado',
            })
        }

        // usuario.id es el id de la bd porque a este punto ya lo encontre por el email y en caso de no haberlo encontrado, cuando lo guardo mas arriba, me devuelve el usuario guardado
        const token = await generarJWT(usuario.id);


        res.json({
            msg: 'Ingreso por google correcto',
            token,
            usuario
        })

    }catch(error) {
        console.log(error)
        return res.status(400).json({
            msg: 'Error al verificar token de google'
        })
    }
}

module.exports = {
    login,
    googleSignIn
}