const { Router, request } = require("express");
const { check } = require("express-validator");
const { usuariosGet, usuariosPost, usuariosPut, usuariosDelete, usuariosPatch } = require("../controllers/usuarios");

const { esRolValido, emailExist, existeUsuarioPorId } = require("../helpers/db-validators");

const {validarCampos, validarJWT, isAdminRole, tieneRole} = require('../middlewares')


const router = Router();

router.get('/', usuariosGet);

// Si defino el parametro como :algo, obligatoriamente se debe mandar ese parametro cada vez que se quiera acceder al endpoint
router.put('/:id',[
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existeUsuarioPorId),
    check('role').custom(esRolValido),
    validarCampos
], usuariosPut);

// en el middleware de validarCampos, toma el req y res gracias al router.post
router.post('/', [
    check('nombre', 'el nombre es obligatorio').not().isEmpty(),
    check('password', 'el password debe ser de minimo 6 caracteres').isLength({min: 6}),
    //check('role', 'No es un rol válido').isIn(['ADMIN_ROLE', 'USER_ROLE']),
    check('role').custom(esRolValido),
    check('correo', 'el correo no es valido').isEmail(),
    check('correo').custom(emailExist),
    validarCampos
], usuariosPost);

router.delete('/:id', [
    validarJWT,
    isAdminRole,
    tieneRole('ADMIN_ROLE', 'NOSE_ROLE'),
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existeUsuarioPorId),
    validarCampos
], usuariosDelete)

router.patch('/', usuariosPatch);


module.exports = router;