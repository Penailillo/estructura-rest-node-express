const {Router} = require('express');
const {check} = require('express-validator');
const { login, googleSignIn } = require('../controllers/auth');
const {emailExist} = require('../helpers/db-validators')
const {validarCampos} = require('../middlewares/validar-campos')

const route = Router();


route.post('/login', [
    //check('correo').custom(emailExist),
    check('password', 'La contraseña es obligatoria').not().isEmpty(),
    check('correo','El email debe tener un formato valido').isEmail(),
    validarCampos
    
] , login);


route.post('/google', [
    //check('correo').custom(emailExist),
    check('id_token', 'Token Obligatorio').not().isEmpty(),
    validarCampos
    
] , googleSignIn);


module.exports = route;